package fr.dauphine.test;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.graphanalyser.GraphAnalyser;
import fr.dauphine.graphreader.GraphReader;
import fr.dauphine.graphreader.GraphWriter;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.query.Query;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Service;

public class Test {

	public static void main(String[] args) {
		
		System.out.println("Graph Initialization");
		System.out.println("Reading graph from text file...");
		Graph<Service, Number> graph = GraphReader.getGraph(args[0], args[1], args[2]);
		System.out.println("done.");
		
		System.out.println("Adding control nodes to graph...");
		Query q = GraphReader.getQuery(args[3]);
		GraphUtils.addControlNodes(graph, q);
		assert(graph.getVertexCount() == 7): 
			"incorrect number of vertices: " + graph.getVertexCount() + "; expected: " + 7;
		assert(graph.getEdgeCount() == 9): 
			"incorrect number of edges: " + graph.getEdgeCount() + "; expected: " + 9;
		
		System.out.println("done.");
		
		System.out.println("Writing graph into text file...");
		GraphWriter.writeGraph(graph, args[0] + "_ifnodes");
		System.out.println("done.");
		
		System.out.println("Graph Analysis");
		GraphAnalyser analyser = new GraphAnalyser(graph);
		
		System.out.println("Calculating graph estimated execution time...");
		double eet = analyser.getEstimatedExecutionTime(Constants.INITIAL_NODE, Constants.FINAL_NODE);
		assert(eet == 14.0): "incorrect estimated execution time: " + eet + "; expected: " + 14.0;
		System.out.println("done. eet = " + eet);
		System.out.println("Calculating graph estimated execution time from each vertex...");
		for(Service s:graph.getVertices())
			if(!s.isControl())
				System.out.println(s.getName() + " = " + analyser.getEstimatedExecutionTime(s.getName(), Constants.FINAL_NODE));
		System.out.println("done.");
		
		System.out.println("Calculating vertex importance...");
		analyser.setVertexImportance();
		System.out.println("done.");
	}

}