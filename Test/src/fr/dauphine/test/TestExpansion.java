package fr.dauphine.test;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.graphreader.GraphReader;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.Service;

public class TestExpansion {

	public static void main(String[] args) {
		
		System.out.println("Graph Initialization");
		System.out.println("Reading graph from text file...");
		Graph<Service, Number> graph = GraphReader.getGraph(args[0], args[1]);
		System.out.println("done.");
		
		System.out.println("Adding control nodes to graph...");
		GraphUtils.addControlNodes(graph);
		System.out.println("done.");
		
		
		System.out.println("Expanding graph from vertex 1...");
		//we create a graph containg only vertex 1
		Graph<Service, Number> expanding = new DirectedSparseGraph<>();
		expanding.addVertex(GraphUtils.getVertexByName(graph, "1"));
		
		assert(expanding.getVertexCount() == 1);
		assert(expanding.getEdgeCount() == 0);
		
		//first expansion
		
		System.out.println("first expansion: " + GraphUtils.expandGraph(graph, expanding));
		assert(expanding.getVertexCount() == 4);
		assert(expanding.getEdgeCount() == 3);
		
		//second expansion
		
		System.out.println("second expansion: " + GraphUtils.expandGraph(graph, expanding));
		assert(expanding.getVertexCount() == 5);
		assert(expanding.getEdgeCount() == 5);
		
		//third expansion is imposible
		System.out.println("third expansion: " + GraphUtils.expandGraph(graph, expanding));
		assert(expanding.getVertexCount() == 5);
		assert(expanding.getEdgeCount() == 5);
		
		System.out.println("done.");
		
		
		
		
		
		

	}

}
